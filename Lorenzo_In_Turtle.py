#032619@LorenzoPedroza Writting My Name with Turtle

from turtle import *     #import everything from turtle library
from math import sqrt

LOWERCASE_LETTER_HEIGHT = 40
O_CIRCLE_RADIUS = LOWERCASE_LETTER_HEIGHT/2
NORMAL_LETTER_SPACE = 10
BASE_Y = 200

def letterSpace(size):
    lorenzoDrawer.penup()
    lorenzoDrawer.setheading(0)
    lorenzoDrawer.forward(size)
    lorenzoDrawer.pendown()

def moveToBottomLine(setPenUp):
    if(setPenUp):
        lorenzoDrawer.penup()
    else:
        lorenzoDrawer.pendown()
    if lorenzoDrawer.ycor() < 200:
        lorenzoDrawer.setheading(90)
    else:
        lorenzoDrawer.setheading(270)
    lorenzoDrawer.forward(abs(BASE_Y-lorenzoDrawer.ycor()))
    lorenzoDrawer.pendown()
    lorenzoDrawer.setheading(0)
        

#initialize
space = Screen()         #create a turtle space
space.title("Lorenzo in Turtle")
lorenzoDrawer = Turtle() #create the turtle that will draw my name
lorenzoDrawer.penup()    #Raise pen before moving to starting location
lorenzoDrawer.goto(-275, 275) #arbitrarily chosen starting point

#Start Drawing

#Draw L
lorenzoDrawer.setheading(270)
lorenzoDrawer.pendown()
lorenzoDrawer.forward(75)
lorenzoDrawer.setheading(0)
lorenzoDrawer.forward(50)

letterSpace(NORMAL_LETTER_SPACE + O_CIRCLE_RADIUS)

#Draw o
lorenzoDrawer.circle(O_CIRCLE_RADIUS)

letterSpace(NORMAL_LETTER_SPACE + O_CIRCLE_RADIUS)

#Draw r
lorenzoDrawer.setheading(90)
lorenzoDrawer.forward(LOWERCASE_LETTER_HEIGHT-5)
lorenzoDrawer.backward(10)
lorenzoDrawer.circle(-15, 180) #semicircule for that segment of R

moveToBottomLine(True)
letterSpace(2*NORMAL_LETTER_SPACE + 0.5*NORMAL_LETTER_SPACE)

#Draw e
lorenzoDrawer.circle(O_CIRCLE_RADIUS, 27.5)
lorenzoDrawer.circle(O_CIRCLE_RADIUS, -27.5)
lorenzoDrawer.circle(O_CIRCLE_RADIUS, -270)
lorenzoDrawer.setheading(180)
lorenzoDrawer.forward(2*O_CIRCLE_RADIUS)

moveToBottomLine(True)
letterSpace(2*O_CIRCLE_RADIUS + NORMAL_LETTER_SPACE)

#Draw n
lorenzoDrawer.setheading(90)
lorenzoDrawer.forward(LOWERCASE_LETTER_HEIGHT-5)
lorenzoDrawer.backward(10)
lorenzoDrawer.circle(-15, 180) #semicircule for that segment of R
moveToBottomLine(False)

letterSpace(NORMAL_LETTER_SPACE)

#Draw z
lorenzoDrawer.forward(LOWERCASE_LETTER_HEIGHT)
lorenzoDrawer.backward(LOWERCASE_LETTER_HEIGHT)
lorenzoDrawer.setheading(45)
lorenzoDrawer.forward(sqrt(2*((2*O_CIRCLE_RADIUS)**2)))
lorenzoDrawer.setheading(180)
lorenzoDrawer.forward(2*O_CIRCLE_RADIUS)

moveToBottomLine(True)
letterSpace(3*O_CIRCLE_RADIUS + NORMAL_LETTER_SPACE)

#Draw o
lorenzoDrawer.circle(O_CIRCLE_RADIUS)
